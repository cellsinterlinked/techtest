# Load balanced vagrant setup with NGINX.

### Getting started
Run these commands in the terminal consecutively
```
vagrant up
vagrant provision
ansible-playbook ansible/playbook.yml --inventory-file ansible/inventories/dev
```
The last command can have the 1 at the end of the IP address swapped for a 2 or a 3 to open the web servers.
Chmod +x scripts such as the vagrantfile, sshpass may need installed in order for the ansible playbook run to ssh onto the machines.

Run the chmod'd automated_test.sh bash script in the terminal. This will give success or failure messages based on whether nginx is provisioned properly or unsuccessfully respectively on the virtual machines.
If the vargantfile proves problematic try swapping "application.vm.synced_folder ".", "/var/www/html""
for "application.vm.synced_folder "./", "/vagrant", :nfs => true, :mount_options => ['vers=3','noatime','actimeo=2', 'tcp', 'fsc']"

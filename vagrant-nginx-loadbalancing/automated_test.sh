#!/bin/bash

code1=$(curl -v --silent http://192.168.30.21/ --stderr -  | grep "head")
message1="* Closing connection 0
<head><title>403 Forbidden</title></head>"

if [ "$code1" == "$message1" ]; then
  printf "there\'s a 403 error with the load loadbalancer"; else printf "nginx is working on the l-b "
fi

code2=$(curl -v --silent http://192.168.30.22/ --stderr -  | grep "head")
message2="* Closing connection 0
<head><title>403 Forbidden</title></head>"

if [ "$code2" == "$message2" ] ;
then
  printf "there\'s a 403 error with the 1st web server "; else printf "nginx is working on the 1st web server "
fi

code3=$(curl -v --silent http://192.168.30.23/ --stderr -  | grep "head")
message3="* Closing connection 0
<head><title>403 Forbidden</title></head>"

if [ "$code3" == "$message3" ] ;
then
  printf "there\'s a 403 error with the 2nd web server "; else printf "nginx is working on the 2nd web server "
fi
